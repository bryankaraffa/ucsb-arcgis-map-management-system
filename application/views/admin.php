
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A layout example with a side menu that hides on mobile, just like the Pure website.">

    <title>Admin Dashboard</title>

<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0/pure-min.css">

<link rel="stylesheet" href="/application/views/css/pure-side-menu.css">

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">

  <style>
  #sortable { list-style-type: none; margin: 0; padding: 0; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1em; }
  #sortable li span.ui-icon-arrowthick-2-n-s { position: absolute; margin-left: -1.3em; }
  #sortable li span.ui-icon-closethick { float: right; cursor:pointer; }
  </style>


</head>
<body>






<div id="layout">
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>

    <div id="menu">
        <div class="pure-menu pure-menu-open">
            <a class="pure-menu-heading" href="http://purecss.io/">Dashboard</a>

            <ul>
            	
                <li><a href="#basic">Basic Settings</a></li>
                <li><a href="#layers">Layers</a></li>


                <li class="menu-item-divided">
                    <a href="/" target="_blank">Visit Map</a>
                </li>
                <li><a href="#">Code Repo</a></li>
            </ul>
        </div>
    </div>

    <div id="main">
    	
        <div class="header">
            <a id="basic"><h1>Basic Settings</h1></a>
            <h2>Some basic stuff to get things right</h2>
        </div>
        <div class="content">
            <h2 class="content-subhead">Site Configuration</h2>
            <form class="pure-form pure-form-aligned" method="post">
			    <fieldset>
			        <div class="pure-control-group">
			            <label for="basic_sitename">Site Name</label>
			            <input id="basic_sitename" type="text" placeholder="My Map" value="<?php echo $basic_sitename ?>" class="pure-input-1-2">
			        </div>
			        <div class="pure-control-group">
			            <label for="basic_contactemail">Contact E-mail</label>
			            <input id="basic_contactemail" type="text" placeholder="icm@geog.ucsb.edu" value="<?php echo $basic_contactemail ?>" class="pure-input-1-2">
			        </div>
			        <div class="pure-control-group">
			            <label for="basic_dashboardpassword">Dashboard Password</label>
			            <input id="basic_dashboardpassword" type="password" class="pure-input-1-2" value="<?php echo $basic_dashboardpassword ?>" >
			        </div>
			    </fieldset>
			</form>
        </div>
        
        <div class="header">
            <a id="layers"><h1>Layer Settings</h1></a>
            <h2>Control which layers are visible on the map</h2>
        </div>
        <div class="content">
            <h2 class="content-subhead">Current Layers</h2>
            <p>
                <ul id="sortable">
				  <?php echo $layer_list ?>
				</ul>
            </p>
        </div>
        <div class="content">
            <h2 class="content-subhead">Add Layer</h2>
            <p>
            <form class="pure-form pure-form-aligned" action="<?php echo $this->config->base_url(); ?>admin/addlayer" method="POST">
			    <fieldset>
			        <div class="pure-control-group">
			        	<label for="layer_addlayer_name">Layer Name (?)</label>
			            <input name="layer_addlayer_name" id="layer_addlayer_name" type="text" placeholder="Example Map" class="pure-input-1-2">
					</div>
			        <div class="pure-control-group">					
			            <label for="layer_addlayer_id">Map ID (?)</label>
			            <input name="layer_addlayer_id" id="layer_addlayer_id" type="text" placeholder="622024f632904a62bbe576bbad9d5448" class="pure-input-1-2">
			        </div>
			        <div class="pure-control-group">
			            <button id="layer_addlayer_btn" type="submit" class="pure-button pure-button-primary pure-button-1">Add Layer</button>
			        </div>
			    </fieldset>
			</form>
					
            </p>
        </div>        
        <div style="position: fixed; top:0px; right:0px;">
            <button id="save_button" type="submit" class="pure-button pure-button-primary" style="height:48px;">
            	Save
            </button>
        </div>
        
    </div>
</div>
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
$(function() {
	$( "#sortable" ).sortable();
  	$( "#sortable" ).disableSelection();
	//End Layers Delete Button onClick.
	
	//Begin Save Config Button
	$("#save_button").click(function() {
		var config = {};
		config['basic_sitename'] = $("#basic_sitename").val();
		config['basic_contactemail'] = $("#basic_contactemail").val();
		config['basic_dashboardpassword']= $("#basic_dashboardpassword").val();
		
		config['layers_order'] = $("#sortable").sortable("toArray");
		
		$.ajax({
		  url: "/admin/saveconfig",
		  data: config,
		  type: 'post',
		  success: function(data) {
		  	console.log(data);
		  	alert('Settings Saved!');
		  }
		});
				
	});
	//End Save Config Button
	// map unique id is retrieved from html elements attributes
	function activateDeleteLayerButton() {  //Made it a function because it needs to be called when we add a new layer via AJAX
		$( "span#layer_deletebtn" ).unbind();
		$( "span#layer_deletebtn" ).click(function() {
	  		var data = new Array();
	  		var layer = $(this).parent().sortable();
	  		$.each($(this).context.attributes, function (){
			  	if (this.name =='webmap_id') {
			  		data['webmap_id'] = this.value;
			  	}
			  	if (this.name =='webmap_name') {
			  		data['webmap_name'] = this.value;
			  	}
			  	if (this.name == 'webmap_mapid') {
			  		data['webmap_mapid'] = this.value;
			  	}
			});
			if (confirm('Are you sure you want to remove '+data['webmap_name']+'? (This cannot be undone.)')) {
				//alert('Deleting webmapid: '+data['webmap_id']);
				$.ajax({
				  dataType: "json",
				  url: "/admin/removelayer/"+data['webmap_id']
				}).done(function(response) {
					if (response.success === true) {
						layer.remove();
					}
					else {
						alert('I\m sorry, there was an issue when trying to remove the layer. Please refresh the page and try again.');
					}
				});
			}
		});
	}
	activateDeleteLayerButton();  	// Run the function once on initialize to activate the delete X's
	//End Layers Delete Button onClick
	
	//Start AddLayer Button onClick
	$("#layer_addlayer_btn").click(function(event) {
		
		if ( $("input#layer_addlayer_name").val().length > 0 && $("input#layer_addlayer_id").val().length > 0 ){
			var name = $("input#layer_addlayer_name").val();
			var mapid = $("input#layer_addlayer_id").val();
			
			$.ajax({
			  dataType: "json",
			  url: "/admin/addlayer/"+encodeURIComponent(name)+"/"+mapid
			}).done(function(response) {
				if (response.success === true) {
					//layer.remove();
					var response='<li class="ui-state-default" id="'+mapid+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+name+'<span id="layer_deletebtn" webmap_id="'+response.id+'" webmap_name="'+name+'" webmap_mapid="'+mapid+'" class="ui-icon ui-icon-closethick"></a></li>';
					
					$("#sortable").append(response);
					activateDeleteLayerButton();
					$("input#layer_addlayer_name").val('');
					$("input#layer_addlayer_id").val('');
					alert('Layer added successfully!');
				}
				else {
					alert('I\m sorry, there was an issue when trying to add the layer. Please refresh the page and try again.');
				}
			});
		}
		else {
			alert('Layer name and Map ID are required!');
			return false;
		}
		event.preventDefault();
	});
	//End AddLayer Button onClick
});
</script>
<script>
(function (window, document) {

    var layout   = document.getElementById('layout'),
        menu     = document.getElementById('menu'),
        menuLink = document.getElementById('menuLink');

    function toggleClass(element, className) {
        var classes = element.className.split(/\s+/),
            length = classes.length,
            i = 0;

        for(; i < length; i++) {
          if (classes[i] === className) {
            classes.splice(i, 1);
            break;
          }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    menuLink.onclick = function (e) {
        var active = 'active';

        e.preventDefault();
        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);
    };

}(this, this.document));
</script>


  <!-- Page rendered in {elapsed_time} -->
</body>
</html>