<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=7,IE=9">
  <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
  <title>Webmap</title>
	<script src="/application/third_party/snapjs/snap.min.js"></script>
	<link rel="stylesheet" href="/application/third_party/snapjs/snap.css">
  <link rel="stylesheet" type="text/css" href="//js.arcgis.com/3.7/js/esri/css/esri.css">
   <link rel="stylesheet" href="/application/third_party/arcgis/map.css">
   <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0/pure-min.css">
  <!-- Load the library references for ArcGIS API for JavaScript-->
  <script src="//js.arcgis.com/3.7compact"></script>
  <script>
    require(["esri/map", 
      "esri/dijit/Scalebar", 
      "esri/dijit/Legend", 
      "esri/arcgis/utils",
      "/application/third_party/arcgis/utils.js",
      "dojo/dom", 
      "dojo/on", 
      "dojo/query", 
      "dojo/domReady!"], 
      function(Map, Scalebar, Legend, esriUtils, utils, dom, on, query) {  
        "use strict"
        
        var map;
        var scalebar;
        var legend;

        function loadBasemap(){
          resetMap();
          // Create new map
          if (typeof map === "undefined") {
          	map = new Map("mapDiv"); // If the map hasn't been made yet, init the object'
          	resetMap();
          }
          
          createMap(document.getElementById('currentlayer').children[0].value);
          //loadWebmap();
          utils.autoRecenter(map);
        }

        // Load basemap map when page loads
        loadBasemap();
    
        // Wire events        
        on(dom.byId("btnClear"),"click", loadBasemap);
        on(dom.byId("currentlayer"),"change", loadWebmap);
        
        // Load webmap
        function loadWebmap(e) {
        	if (dom.byId("currentlayer").value.trim() == '' || dom.byId("currentlayer").value.trim() == null) {
        		return;
        	}
          
          // Clear old map
          resetMap();
		  createMap(dom.byId("currentlayer").value.trim());          
        }
        
        function createMap(mapid) {
			// Get new webmap and extract map and map parts
	          console.log('Loading webmap: '+mapid);
	          var mapDeferred = esriUtils.createMap(mapid, "mapDiv", {
	            mapOptions: {
	              slider: true,
	              nav:false
	            }
	          });
	
	          mapDeferred.then(function(response) {   
	            map = response.map;
	            utils.autoRecenter(map);
	            // Add titles
	            dom.byId("mapTitle").innerHTML = response.itemInfo.item.title;
	            dom.byId("mapSubTitle").innerHTML = response.itemInfo.item.snippet;
	            // Add scalebar and legend
	            var layers = esri.arcgis.utils.getLegendLayers(response);  
	            if(map.loaded){
	              initMapParts(layers);
	            }
	            else{
	              on(map,"load",function(){
	                initMapParts(layers);
	              });
	            }
	          },function(error){
	            alert("Sorry, couldn't load webmap!");
	            console.log("Error loading webmap: " & dojo.toJson(error));           
	          });        	
        }
        
        function initMapParts(layers){
         //add a scalebar
          scalebar = new Scalebar({
            map:map,
            scalebarUnit: 'english'
          });
          //add a legend
          if (legend) {
            legend.map = map;
            legend.refresh(layers);
          }
          else {
            legend = new Legend({
                map:map,
                layerInfos:layers
              },"mapLegend");
            legend.startup();
          }
        }

        // Clean up map
        function resetMap() {
          if (map) { 
            map.removeAllLayers();
            map.spatialReference = null;
            map.destroy();
          }
          if (scalebar)
           scalebar.destroy();
          if (legend) {
            dom.byId("mapLegend").innerHTML = "";
          }        
          dom.byId("mapTitle").innerHTML = "";
          dom.byId("mapSubTitle").innerHTML = "";
        }
      }
    );
  </script>
</head>
<body>
  <div class="panel">      
  	<button id="leftpaneltoggle" class="btn">Menu</button>
  </div><!--End Load Webmap Div-->
  
  
  	<div class="snap-drawers">
            <div class="snap-drawer snap-drawer-left">
	          <div id="title" class="titlearea"><span id="subtitle" class="title-message">Load Webmap</span></div>
		    <form class="pure-form pure-form-stacked">
			    <fieldset>
			        <div class="pure-g">
			            <div class="pure-u-1">
			                <select id="currentlayer" class="pure-input-1" style="height:35px;">
			                	<!--<option value=''>Choose a map</option>-->
			                    <?php echo $layersmenu ?>
			                </select>
			            </div>
			        </div>
		
			    </fieldset>
			</form>
		      <div class="controls">
		        <div class="buttons">
		          <!--<input id="webmapInput" rows="3" 
		            placeholder="622024f632904a62bbe576bbad9d5448"
		            value="622024f632904a62bbe576bbad9d5448">          
		          </input>
		          <button id="btnWebmap" class="btn btn-primary">Add</button>-->
		          <button id="btnClear" class="btn">Reset Map</button>
		        </div>
		      </div>
            	<div id="infoPanel" class="info-panel">
			        <div id="mapTitle" lass="map-title"></div>
			        <div id="mapSubTitle" class="map-subtitle"></div>
			        <div id="mapLegend"></div>
			      </div>
            </div>
	</div>
	<div id="content" class="snap-content">
		<div id="mapDiv"></div>
  		<div id="progress" class="progress hidden"></div>
	</div>
  
  
	<script>
	var snapper = new Snap({
	  element: document.getElementById('content'),
	  slideIntent: 5,
	  hyperextensible: false,
	  disable: 'right',
	  touchToDrag: false,
	  minDragDistance: 25
		});
		snapper.leftpanelstate = false;
		document.getElementById('leftpaneltoggle').addEventListener('click', function() {
			if (snapper.leftpanelstate) {
				snapper.close('left');
				snapper.leftpanelstate = !snapper.leftpanelstate;
			}
			else {
				snapper.open('left');				
				snapper.leftpanelstate = !snapper.leftpanelstate;
			}
		})
	</script>
  
  <!-- Page rendered in {elapsed_time} -->
</body>
</html>
 