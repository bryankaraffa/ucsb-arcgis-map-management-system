<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['layersmenu'] = $this->_formatLayersList();
		$this->load->view('map', $data);
	}
	public function _formatLayersList() {
		$this->load->database();
		$response='';
		$query = $this->db->query('SELECT name, webmap_id FROM layers ORDER BY `order` ASC');
		foreach ($query->result_array() as $layer) {
			$response.='<option value="'.$layer['webmap_id'].'">'.$layer['name'].'</option>';
		}
		return($response);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */