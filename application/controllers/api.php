<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/REST_Controller.php');  

class Api extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index_get() {
		 $this->response( 'This is where the API doc will go.');
	}
	public function internal_get() {	
		if ($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']){
			//Continue with internal
		}
		else {
			//$this->output->set_status_header(400, 'No Remote Access Allowed');
			//$_SERVER['SERVER_ADDR']." != ".$_SERVER['REMOTE_ADDR'];
			 $this->response('No Remote Access Allowed',400);
			exit; //just for good measure
		}
	}
	public function getlayers_get() {
		$this->load->database();
			
		$query = $this->db->query('SELECT name, webmap_id FROM layers');
		$this->response($query->result_array());
	}
	public function getlayer_get($layerid) {
		$this->load->database();
		$query = $this->db->query('SELECT name, webmap_id FROM layers WHERE `name`=\''.$this->db->escape_str($layerid).'\'');
		if ($query->num_rows() > 0) {
			$this->response($query->result_array());
		}
		else {
			$this->response('Layer not found.');
		}
	}
}
/* End of file api.php */
/* Location: ./application/controllers/api.php */