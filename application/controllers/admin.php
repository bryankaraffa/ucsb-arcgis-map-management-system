<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index(){
		$data['layer_list'] = $this->_formatLayersList();
		foreach($this->_getConfigValues() as $key => $value) {
			$data[$key] = $value;
		}		
		$this->load->helper('form');
		$this->load->view('admin', $data);
	}
	public function addlayer($name,$mapid){
		$this->load->database();
		$data = array(
               'name' => urldecode($name),
               'webmap_id' => $mapid
            );

		if ($this->db->insert('layers', $data)) { echo('{"success":true, "id":'.$this->db->insert_id().'}'); }
		else { echo('{"success":false}'); }	
	}
	public function removelayer($id){
		$this->load->database();
		$data = array(
               'id' => $id
            );

		if ($this->db->delete('layers', $data)) { echo('{"success":true}'); }
		else {
			echo '{"success":false}';
		}
		
		//$this->load->helper('url');
		//redirect('admin', 'location', 301);
	}
	public function saveConfig() {
		if ($this->input->post()) {
			$this->load->database();
			$config = $this->input->post();
			//print_r($config);
			$settings = Array();
			$layerorder = Array();
			
			foreach ($config as $key => $value) {
				if ($key == 'layers_order') {
					foreach ($value as $order => $id) {
						array_push($layerorder, array('id' => $id, 'order'=>$order));
					}
					//print_r($value);
				}
				else {
					if ($key == 'basic_dashboardpassword') {
						$value = $value;
					}
					array_push($settings, array('key' => $key, 'value'=>$value));	
				}				
			}
			$this->db->update_batch('layers', $layerorder, 'id');
			$this->db->update_batch('settings', $settings, 'key');
			//if (true == false) { echo '{"success":true}'; }
			//else { echo '{"success":false}'; }	
		}				
	}
	public function _getConfigValues() {
		$this->load->database();
		$response=Array();
		$query = $this->db->get('settings');
		foreach ($query->result() as $row) {
		    $response[$row->key]=$row->value;
		}
		return($response);
	}
	public function _formatLayersList() {
		$this->load->database();
		$response='';
		$query = $this->db->query('SELECT id, name, webmap_id FROM layers ORDER BY `order` ASC');
		foreach ($query->result_array() as $layer) {
			$response.='<li class="ui-state-default" id="'.$layer['id'].'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'.$layer['name'].'<span id="layer_deletebtn" webmap_id="'.$layer['id'].'" webmap_name="'.$layer['name'].'" webmap_mapid="'.$layer['webmap_id'].'" class="ui-icon ui-icon-closethick"></a></li>';
			//$response.='<option value="'.$layer['webmap_id'].'">'.$layer['name'].'</option>';
		}
		return($response);
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */