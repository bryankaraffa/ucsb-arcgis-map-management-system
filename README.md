## Description
#### A map management application that uses the ArcGIS Online Hosting platform.  Provides an administrative dashboard for configuring the map.

#### Original idea and structure stemmed from what was learned during development on the [UCSB Interactive Campus Map](http://map.geog.ucsb.edu/).  This application is a recreation of the core functionality of the UCSB ICM rewritten to utilize ArcGIS Online, a PHP Framework, JQuery, with a mobile-first, minimal approach.

----

## Credits / Projects Used

#### [CodeIgniter](http://ellislab.com/codeigniter) - PHP Framework which simplifies a lot of redundant tasks in PHP development.
#### [ArcGIS API for JavaScript](https://developers.arcgis.com/en/javascript/) - A very powerful web mapping library
#### [Pure CSS](http://purecss.io/) - Makes everything look great!  
#### [codeigniter-restserver](https://github.com/philsturgeon/codeigniter-restserver) - Special REST Controller which powers the API



## Development Funded By 
The [Cheadle Center for Biodiversity and Ecological Restoration (CCBER)](http://ccber.ucsb.edu) in association with the [UCSB Geography Department](http://geog.ucsb.edu/)